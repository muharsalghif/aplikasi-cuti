/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import model.Akun;

/**
 *
 * @author GustavSR1994
 */
public interface AkunDAO {
    public List<Akun> getAllAkun();
    public Akun getById(long id);
    public Akun getLogin(String usernm, String passw);
    
}
