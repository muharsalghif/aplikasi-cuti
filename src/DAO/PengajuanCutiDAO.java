/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import model.PengajuanCuti;

/**
 *
 * @author GustavSR1994
 */
public interface PengajuanCutiDAO {
    public List<PengajuanCuti> getAllPengajuanCuti();
    public PengajuanCuti getById(long id);
    public List<PengajuanCuti> getByKodeKaryawan(long kodeAkun);
    public List<PengajuanCuti> getByKodeDepartment(long kodeDepartment);
    
}
