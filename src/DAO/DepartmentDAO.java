/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import model.Department;

/**
 *
 * @author GustavSR1994
 */
public interface DepartmentDAO {
    public List<Department> getAllDepartment();
    public Department getById(long id);
}
