/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author GustavSR1994
 */
public interface GeneralDAO {
    public void insert(Object o);
    public void delete(Object o);
    public void update(Object o);
}
