/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import model.Jabatan;
/**
 *
 * @author GustavSR1994
 */
public interface JabatanDAO {
    public List<Jabatan> getAllJabatan();
    public Jabatan getById(long id);
}
