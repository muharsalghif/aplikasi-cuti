/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import model.Provinsi;
/**
 *
 * @author GustavSR1994
 */
public interface ProvinsiDAO {
    public List<Provinsi> getAllProvinsi();
    public Provinsi getById(long id);
}
