/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.DepartmentDAO;
import DB.DBConnection;
import java.util.List;
import javax.persistence.EntityManager;
import model.Department;

/**
 *
 * @author GustavSR1994
 */
public class DepartmentDAOImpl extends GeneralDAOImpl implements DepartmentDAO{
    EntityManager em;
   
    public DepartmentDAOImpl(){
        em = DBConnection.getConnection();
    }
    
    @Override
    public List<Department> getAllDepartment() {
        return em.createQuery("SELECT d FROM Department d").getResultList();
    }

    @Override
    public Department getById(long id) {
        return em.find(Department.class, id);
    }
    
}
