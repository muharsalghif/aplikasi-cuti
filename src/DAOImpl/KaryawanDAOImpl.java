/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.KaryawanDAO;
import DB.DBConnection;
import java.util.List;
import javax.persistence.EntityManager;
import model.Karyawan;

/**
 *
 * @author GustavSR1994
 */
public class KaryawanDAOImpl extends GeneralDAOImpl implements KaryawanDAO{
    EntityManager em;
   
    public KaryawanDAOImpl(){
        em = DBConnection.getConnection();
    }
    @Override
    public List<Karyawan> getAllKaryawan() {
        return em.createQuery("SELECT k FROM Karyawan k").getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Karyawan getById(long id) {
        return em.find(Karyawan.class, id); //To change body of generated methods, choose Tools | Templates.
    }
    
}
