/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.JenisCutiDAO;
import DB.DBConnection;
import java.util.List;
import javax.persistence.EntityManager;
import model.JenisCuti;

/**
 *
 * @author GustavSR1994
 */
public class JenisCutiDAOImpl implements JenisCutiDAO{
    EntityManager em;
   
    public JenisCutiDAOImpl(){
        em = DBConnection.getConnection();
    }
    @Override
    public List<JenisCuti> getAllJenisCuti() {
        return em.createQuery("SELECT je FROM JenisCuti je").getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public JenisCuti getById(long id) {
        return em.find(JenisCuti.class, id);//To change body of generated methods, choose Tools | Templates.
    }
    
}
