/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.PengajuanCutiDAO;
import DB.DBConnection;
import java.util.List;
import javax.persistence.EntityManager;
import model.PengajuanCuti;

/**
 *
 * @author GustavSR1994
 */
public class PengajuanCutiDAOImpl extends GeneralDAOImpl implements PengajuanCutiDAO{
    EntityManager em;
   
    public PengajuanCutiDAOImpl(){
        em = DBConnection.getConnection();
    }
    @Override
    public List<PengajuanCuti> getAllPengajuanCuti() {
        return em.createQuery("SELECT pe FROM PengajuanCuti pe").getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PengajuanCuti getById(long id) {
        return em.find(PengajuanCuti.class, id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PengajuanCuti> getByKodeKaryawan(long kodeAkun) {
        return em.createQuery("SELECT pe FROM PengajuanCuti pe where pe.kodeAkun.kodeAkun = ?1")
                .setParameter(1, kodeAkun)
                .getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PengajuanCuti> getByKodeDepartment(long kodeDepartment) {
        return em.createQuery("SELECT pe FROM PengajuanCuti pe where pe.kodeDepartment.kodeDepartment = ?1")
                .setParameter(1, kodeDepartment)
                .getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

}
