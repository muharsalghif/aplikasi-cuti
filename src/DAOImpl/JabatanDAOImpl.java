/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.JabatanDAO;
import DB.DBConnection;
import java.util.List;
import javax.persistence.EntityManager;
import model.Jabatan;

/**
 *
 * @author GustavSR1994
 */
public class JabatanDAOImpl extends GeneralDAOImpl implements JabatanDAO{

    EntityManager em;
   
    public JabatanDAOImpl(){
        em = DBConnection.getConnection();
    }
    
    @Override
    public List<Jabatan> getAllJabatan() {
        return em.createQuery("SELECT ja FROM Jabatan ja").getResultList();
    }

    @Override
    public Jabatan getById(long id) {
        return em.find(Jabatan.class, id); //To change body of generated methods, choose Tools | Templates.
    }
    
}
