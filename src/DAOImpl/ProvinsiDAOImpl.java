/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.ProvinsiDAO;
import java.util.List;
import model.Provinsi;

/**
 *
 * @author GustavSR1994
 */
public class ProvinsiDAOImpl extends GeneralDAOImpl implements ProvinsiDAO{

    @Override
    public List<Provinsi> getAllProvinsi() {
        return em.createQuery("SELECT pr FROM Provinsi pr").getResultList(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Provinsi getById(long id) {
        return em.find(Provinsi.class, id); //To change body of generated methods, choose Tools | Templates.
    }
    
}
