/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOImpl;

import DAO.AkunDAO;
import DB.DBConnection;
import enskrpsi.Enkripsi;
import java.util.List;
import javax.persistence.EntityManager;
import model.Akun;

/**
 *
 * @author GustavSR1994
 */
public class AkunDAOImpl extends GeneralDAOImpl implements AkunDAO{
    EntityManager em;
   
    public AkunDAOImpl(){
        em = DBConnection.getConnection();
    }
    @Override
    public List<Akun> getAllAkun() {
        return em.createQuery("SELECT a FROM Akun a").getResultList();
    }

    @Override
    public Akun getById(long id) {
        return em.find(Akun.class, id);
    }

    @Override
    public Akun getLogin(String usernm, String passw) {
        return (Akun)em.createQuery("SELECT a FROM Akun a WHERE a.username = ?1 and a.password = ?2")
                .setParameter(1, usernm)
                .setParameter(2, passw)
                .getSingleResult();
    }
    
}
