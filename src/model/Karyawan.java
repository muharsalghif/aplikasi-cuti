/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GustavSR1994
 */
@Entity
@Table(name = "KARYAWAN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Karyawan.findAll", query = "SELECT k FROM Karyawan k")
    , @NamedQuery(name = "Karyawan.findByKodeKaryawan", query = "SELECT k FROM Karyawan k WHERE k.kodeKaryawan = :kodeKaryawan")
    , @NamedQuery(name = "Karyawan.findByNamaKaryawan", query = "SELECT k FROM Karyawan k WHERE k.namaKaryawan = :namaKaryawan")
    , @NamedQuery(name = "Karyawan.findByTempatLahir", query = "SELECT k FROM Karyawan k WHERE k.tempatLahir = :tempatLahir")
    , @NamedQuery(name = "Karyawan.findByTglLahir", query = "SELECT k FROM Karyawan k WHERE k.tglLahir = :tglLahir")
    , @NamedQuery(name = "Karyawan.findByAlamat", query = "SELECT k FROM Karyawan k WHERE k.alamat = :alamat")
    , @NamedQuery(name = "Karyawan.findByTglAwalKerja", query = "SELECT k FROM Karyawan k WHERE k.tglAwalKerja = :tglAwalKerja")})
public class Karyawan implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "KODE_KARYAWAN")
    private long kodeKaryawan;
    @Basic(optional = false)
    @Column(name = "NAMA_KARYAWAN")
    private String namaKaryawan;
    @Basic(optional = false)
    @Column(name = "TEMPAT_LAHIR")
    private String tempatLahir;
    @Basic(optional = false)
    @Column(name = "TGL_LAHIR")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglLahir;
    @Basic(optional = false)
    @Column(name = "ALAMAT")
    private String alamat;
    @Basic(optional = false)
    @Column(name = "TGL_AWAL_KERJA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglAwalKerja;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kodeKaryawan")
    private List<Akun> akunList;
    @JoinColumn(name = "KODE_DEPARTMENT", referencedColumnName = "KODE_DEPARTMENT")
    @ManyToOne(optional = false)
    private Department kodeDepartment;
    @JoinColumn(name = "KODE_JABATAN", referencedColumnName = "KODE_JABATAN")
    @ManyToOne(optional = false)
    private Jabatan kodeJabatan;
    @JoinColumn(name = "KODE_PROVINSI", referencedColumnName = "KODE_PROVINSI")
    @ManyToOne(optional = false)
    private Provinsi kodeProvinsi;

    public Karyawan() {
    }

    public Karyawan(long kodeKaryawan) {
        this.kodeKaryawan = kodeKaryawan;
    }

    public Karyawan(long kodeKaryawan, String namaKaryawan, String tempatLahir, Date tglLahir, String alamat, Date tglAwalKerja) {
        this.kodeKaryawan = kodeKaryawan;
        this.namaKaryawan = namaKaryawan;
        this.tempatLahir = tempatLahir;
        this.tglLahir = tglLahir;
        this.alamat = alamat;
        this.tglAwalKerja = tglAwalKerja;
    }

    public long getKodeKaryawan() {
        return kodeKaryawan;
    }

    public void setKodeKaryawan(long kodeKaryawan) {
        this.kodeKaryawan = kodeKaryawan;
    }

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public void setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public Date getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(Date tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Date getTglAwalKerja() {
        return tglAwalKerja;
    }

    public void setTglAwalKerja(Date tglAwalKerja) {
        this.tglAwalKerja = tglAwalKerja;
    }

    @XmlTransient
    public List<Akun> getAkunList() {
        return akunList;
    }

    public void setAkunList(List<Akun> akunList) {
        this.akunList = akunList;
    }

    public Department getKodeDepartment() {
        return kodeDepartment;
    }

    public void setKodeDepartment(Department kodeDepartment) {
        this.kodeDepartment = kodeDepartment;
    }

    public Jabatan getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(Jabatan kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public Provinsi getKodeProvinsi() {
        return kodeProvinsi;
    }

    public void setKodeProvinsi(Provinsi kodeProvinsi) {
        this.kodeProvinsi = kodeProvinsi;
    }

    @Override
    public String toString() {
        return ""+ kodeKaryawan;
    }
    
}
