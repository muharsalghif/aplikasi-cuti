/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GustavSR1994
 */
@Entity
@Table(name = "PENGAJUAN_CUTI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PengajuanCuti.findAll", query = "SELECT p FROM PengajuanCuti p")
    , @NamedQuery(name = "PengajuanCuti.findByKodeCuti", query = "SELECT p FROM PengajuanCuti p WHERE p.kodeCuti = :kodeCuti")
    , @NamedQuery(name = "PengajuanCuti.findByTglPengajuan", query = "SELECT p FROM PengajuanCuti p WHERE p.tglPengajuan = :tglPengajuan")
    , @NamedQuery(name = "PengajuanCuti.findByTglMulaiCuti", query = "SELECT p FROM PengajuanCuti p WHERE p.tglMulaiCuti = :tglMulaiCuti")
    , @NamedQuery(name = "PengajuanCuti.findByTglAkhirCuti", query = "SELECT p FROM PengajuanCuti p WHERE p.tglAkhirCuti = :tglAkhirCuti")
    , @NamedQuery(name = "PengajuanCuti.findByStatus", query = "SELECT p FROM PengajuanCuti p WHERE p.status = :status")})
public class PengajuanCuti implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "KODE_CUTI")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //@GenericGenerator(name ="increment")
    private long kodeCuti;
    @Basic(optional = false)
    @Column(name = "TGL_PENGAJUAN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglPengajuan;
    @Basic(optional = false)
    @Column(name = "TGL_MULAI_CUTI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglMulaiCuti;
    @Basic(optional = false)
    @Column(name = "TGL_AKHIR_CUTI")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglAkhirCuti;
    @Basic(optional = false)
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "KODE_AKUN", referencedColumnName = "KODE_AKUN")
    @ManyToOne(optional = false)
    private Akun kodeAkun;
    @JoinColumn(name = "KODE_DEPARTMENT", referencedColumnName = "KODE_DEPARTMENT")
    @ManyToOne(optional = false)
    private Department kodeDepartment;
    @JoinColumn(name = "KODE_JENIS", referencedColumnName = "KODE_JENIS")
    @ManyToOne(optional = false)
    private JenisCuti kodeJenis;

    public PengajuanCuti() {
    }

    public PengajuanCuti(long kodeCuti) {
        this.kodeCuti = kodeCuti;
    }

    public PengajuanCuti(long kodeCuti, Date tglPengajuan, Date tglMulaiCuti, Date tglAkhirCuti, String status) {
        this.kodeCuti = kodeCuti;
        this.tglPengajuan = tglPengajuan;
        this.tglMulaiCuti = tglMulaiCuti;
        this.tglAkhirCuti = tglAkhirCuti;
        this.status = status;
    }

    public long getKodeCuti() {
        return kodeCuti;
    }

    public void setKodeCuti(long kodeCuti) {
        long oldKodeCuti = this.kodeCuti;
        this.kodeCuti = kodeCuti;
        changeSupport.firePropertyChange("kodeCuti", oldKodeCuti, kodeCuti);
    }

    public Date getTglPengajuan() {
        return tglPengajuan;
    }

    public void setTglPengajuan(Date tglPengajuan) {
        Date oldTglPengajuan = this.tglPengajuan;
        this.tglPengajuan = tglPengajuan;
        changeSupport.firePropertyChange("tglPengajuan", oldTglPengajuan, tglPengajuan);
    }

    public Date getTglMulaiCuti() {
        return tglMulaiCuti;
    }

    public void setTglMulaiCuti(Date tglMulaiCuti) {
        Date oldTglMulaiCuti = this.tglMulaiCuti;
        this.tglMulaiCuti = tglMulaiCuti;
        changeSupport.firePropertyChange("tglMulaiCuti", oldTglMulaiCuti, tglMulaiCuti);
    }

    public Date getTglAkhirCuti() {
        return tglAkhirCuti;
    }

    public void setTglAkhirCuti(Date tglAkhirCuti) {
        Date oldTglAkhirCuti = this.tglAkhirCuti;
        this.tglAkhirCuti = tglAkhirCuti;
        changeSupport.firePropertyChange("tglAkhirCuti", oldTglAkhirCuti, tglAkhirCuti);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        String oldStatus = this.status;
        this.status = status;
        changeSupport.firePropertyChange("status", oldStatus, status);
    }

    public Akun getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(Akun kodeAkun) {
        Akun oldKodeAkun = this.kodeAkun;
        this.kodeAkun = kodeAkun;
        changeSupport.firePropertyChange("kodeAkun", oldKodeAkun, kodeAkun);
    }

    public Department getKodeDepartment() {
        return kodeDepartment;
    }

    public void setKodeDepartment(Department kodeDepartment) {
        Department oldKodeDepartment = this.kodeDepartment;
        this.kodeDepartment = kodeDepartment;
        changeSupport.firePropertyChange("kodeDepartment", oldKodeDepartment, kodeDepartment);
    }

    public JenisCuti getKodeJenis() {
        return kodeJenis;
    }

    public void setKodeJenis(JenisCuti kodeJenis) {
        JenisCuti oldKodeJenis = this.kodeJenis;
        this.kodeJenis = kodeJenis;
        changeSupport.firePropertyChange("kodeJenis", oldKodeJenis, kodeJenis);
    }

    @Override
    public String toString() {
        return "model.PengajuanCuti[ kodeCuti=" + kodeCuti + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    
}
