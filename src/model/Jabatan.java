/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GustavSR1994
 */
@Entity
@Table(name = "JABATAN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Jabatan.findAll", query = "SELECT j FROM Jabatan j")
    , @NamedQuery(name = "Jabatan.findByKodeJabatan", query = "SELECT j FROM Jabatan j WHERE j.kodeJabatan = :kodeJabatan")
    , @NamedQuery(name = "Jabatan.findByNamaJabatan", query = "SELECT j FROM Jabatan j WHERE j.namaJabatan = :namaJabatan")})
public class Jabatan implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "KODE_JABATAN")
    private long kodeJabatan;
    @Basic(optional = false)
    @Column(name = "NAMA_JABATAN")
    private String namaJabatan;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kodeJabatan")
    private List<Karyawan> karyawanList;

    public Jabatan() {
    }

    public Jabatan(long kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public Jabatan(long kodeJabatan, String namaJabatan) {
        this.kodeJabatan = kodeJabatan;
        this.namaJabatan = namaJabatan;
    }

    public long getKodeJabatan() {
        return kodeJabatan;
    }

    public void setKodeJabatan(long kodeJabatan) {
        this.kodeJabatan = kodeJabatan;
    }

    public String getNamaJabatan() {
        return namaJabatan;
    }

    public void setNamaJabatan(String namaJabatan) {
        this.namaJabatan = namaJabatan;
    }

    @XmlTransient
    public List<Karyawan> getKaryawanList() {
        return karyawanList;
    }

    public void setKaryawanList(List<Karyawan> karyawanList) {
        this.karyawanList = karyawanList;
    }

    @Override
    public String toString() {
        return "model.Jabatan[ kodeJabatan=" + kodeJabatan + " ]";
    }
    
}
