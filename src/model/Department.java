/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GustavSR1994
 */
@Entity
@Table(name = "DEPARTMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Department.findAll", query = "SELECT d FROM Department d")
    , @NamedQuery(name = "Department.findByKodeDepartment", query = "SELECT d FROM Department d WHERE d.kodeDepartment = :kodeDepartment")
    , @NamedQuery(name = "Department.findByNamaDepartment", query = "SELECT d FROM Department d WHERE d.namaDepartment = :namaDepartment")})
public class Department implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kodeDepartment")
    private List<PengajuanCuti> pengajuanCutiList;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "KODE_DEPARTMENT")
    private long kodeDepartment;
    @Basic(optional = false)
    @Column(name = "NAMA_DEPARTMENT")
    private String namaDepartment;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kodeDepartment")
    private List<Karyawan> karyawanList;

    public Department() {
    }

    public Department(long kodeDepartment) {
        this.kodeDepartment = kodeDepartment;
    }

    public Department(long kodeDepartment, String namaDepartment) {
        this.kodeDepartment = kodeDepartment;
        this.namaDepartment = namaDepartment;
    }

    public long getKodeDepartment() {
        return kodeDepartment;
    }

    public void setKodeDepartment(long kodeDepartment) {
        this.kodeDepartment = kodeDepartment;
    }

    public String getNamaDepartment() {
        return namaDepartment;
    }

    public void setNamaDepartment(String namaDepartment) {
        this.namaDepartment = namaDepartment;
    }

    @XmlTransient
    public List<Karyawan> getKaryawanList() {
        return karyawanList;
    }

    public void setKaryawanList(List<Karyawan> karyawanList) {
        this.karyawanList = karyawanList;
    }


    @Override
    public String toString() {
        return "model.Department[ kodeDepartment=" + kodeDepartment + " ]";
    }

    @XmlTransient
    public List<PengajuanCuti> getPengajuanCutiList() {
        return pengajuanCutiList;
    }

    public void setPengajuanCutiList(List<PengajuanCuti> pengajuanCutiList) {
        this.pengajuanCutiList = pengajuanCutiList;
    }
    
}
