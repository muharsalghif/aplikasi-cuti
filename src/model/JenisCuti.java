/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GustavSR1994
 */
@Entity
@Table(name = "JENIS_CUTI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JenisCuti.findAll", query = "SELECT j FROM JenisCuti j")
    , @NamedQuery(name = "JenisCuti.findByKodeJenis", query = "SELECT j FROM JenisCuti j WHERE j.kodeJenis = :kodeJenis")
    , @NamedQuery(name = "JenisCuti.findByNamaCuti", query = "SELECT j FROM JenisCuti j WHERE j.namaCuti = :namaCuti")})
public class JenisCuti implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "KODE_JENIS")
    private long kodeJenis;
    @Basic(optional = false)
    @Column(name = "NAMA_CUTI")
    private String namaCuti;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kodeJenis")
    private List<PengajuanCuti> pengajuanCutiList;

    public JenisCuti() {
    }

    public JenisCuti(long kodeJenis) {
        this.kodeJenis = kodeJenis;
    }

    public JenisCuti(long kodeJenis, String namaCuti) {
        this.kodeJenis = kodeJenis;
        this.namaCuti = namaCuti;
    }

    public long getKodeJenis() {
        return kodeJenis;
    }

    public void setKodeJenis(long kodeJenis) {
        this.kodeJenis = kodeJenis;
    }

    public String getNamaCuti() {
        return namaCuti;
    }

    public void setNamaCuti(String namaCuti) {
        this.namaCuti = namaCuti;
    }

    @XmlTransient
    public List<PengajuanCuti> getPengajuanCutiList() {
        return pengajuanCutiList;
    }

    public void setPengajuanCutiList(List<PengajuanCuti> pengajuanCutiList) {
        this.pengajuanCutiList = pengajuanCutiList;
    }

    @Override
    public String toString() {
        return "model.JenisCuti[ kodeJenis=" + kodeJenis + " ]";
    }
    
}
