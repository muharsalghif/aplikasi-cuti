/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GustavSR1994
 */
@Entity
@Table(name = "PROVINSI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Provinsi.findAll", query = "SELECT p FROM Provinsi p")
    , @NamedQuery(name = "Provinsi.findByKodeProvinsi", query = "SELECT p FROM Provinsi p WHERE p.kodeProvinsi = :kodeProvinsi")
    , @NamedQuery(name = "Provinsi.findByProvinsi", query = "SELECT p FROM Provinsi p WHERE p.provinsi = :provinsi")})
public class Provinsi implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "KODE_PROVINSI")
    private BigDecimal kodeProvinsi;
    @Basic(optional = false)
    @Column(name = "PROVINSI")
    private String provinsi;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kodeProvinsi")
    private List<Karyawan> karyawanList;

    public Provinsi() {
    }

    public Provinsi(BigDecimal kodeProvinsi) {
        this.kodeProvinsi = kodeProvinsi;
    }

    public Provinsi(BigDecimal kodeProvinsi, String provinsi) {
        this.kodeProvinsi = kodeProvinsi;
        this.provinsi = provinsi;
    }

    public BigDecimal getKodeProvinsi() {
        return kodeProvinsi;
    }

    public void setKodeProvinsi(BigDecimal kodeProvinsi) {
        this.kodeProvinsi = kodeProvinsi;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    @XmlTransient
    public List<Karyawan> getKaryawanList() {
        return karyawanList;
    }

    public void setKaryawanList(List<Karyawan> karyawanList) {
        this.karyawanList = karyawanList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kodeProvinsi != null ? kodeProvinsi.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provinsi)) {
            return false;
        }
        Provinsi other = (Provinsi) object;
        if ((this.kodeProvinsi == null && other.kodeProvinsi != null) || (this.kodeProvinsi != null && !this.kodeProvinsi.equals(other.kodeProvinsi))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Provinsi[ kodeProvinsi=" + kodeProvinsi + " ]";
    }
    
}
