/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GustavSR1994
 */
@Entity
@Table(name = "AKUN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Akun.findAll", query = "SELECT a FROM Akun a")
    , @NamedQuery(name = "Akun.findByKodeAkun", query = "SELECT a FROM Akun a WHERE a.kodeAkun = :kodeAkun")
    , @NamedQuery(name = "Akun.findByUsername", query = "SELECT a FROM Akun a WHERE a.username = :username")
    , @NamedQuery(name = "Akun.findByPassword", query = "SELECT a FROM Akun a WHERE a.password = :password")})
public class Akun implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "KODE_AKUN")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long kodeAkun;
    @Basic(optional = false)
    @Column(name = "USERNAME", unique = true)
    private String username;
    @Basic(optional = false)
    @Column(name = "PASSWORD")
    private String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kodeAkun")
    private List<PengajuanCuti> pengajuanCutiList;
    @JoinColumn(name = "KODE_KARYAWAN", referencedColumnName = "KODE_KARYAWAN")
    @ManyToOne(optional = false)
    private Karyawan kodeKaryawan;

    public Akun() {
    }

    public Akun(long kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    public Akun(long kodeAkun, String username, String password) {
        this.kodeAkun = kodeAkun;
        this.username = username;
        this.password = password;
    }

    public long getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(long kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public List<PengajuanCuti> getPengajuanCutiList() {
        return pengajuanCutiList;
    }

    public void setPengajuanCutiList(List<PengajuanCuti> pengajuanCutiList) {
        this.pengajuanCutiList = pengajuanCutiList;
    }

    public Karyawan getKodeKaryawan() {
        return kodeKaryawan;
    }

    public void setKodeKaryawan(Karyawan kodeKaryawan) {
        this.kodeKaryawan = kodeKaryawan;
    }

    @Override
    public String toString() {
        return "model.Akun[ kodeAkun=" + kodeAkun + " ]";
    }

    public void setKodeAkun() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
